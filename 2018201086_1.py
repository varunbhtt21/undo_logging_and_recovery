#!/usr/bin/env python
# coding: utf-8

# In[470]:


import sys

# fname="input.txt"
output="2018201086_1.txt"

fname = sys.argv[1]
x = int(sys.argv[2])

with open(fname) as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content] 

print(content)


# In[471]:


content_updated = []

for i in range(len(content)):
    if content[i] != '':
        content_updated.append(content[i])

print(content_updated)


# In[472]:


Trans = {}
Memory = []
HDD = content_updated[0].split(" ")

content_updated.remove(content_updated[0])
print(content_updated)
print(HDD)


# In[473]:


i = 0
print(len(content_updated))

while i<len(content_updated):
    val = int(content_updated[i].split(" ")[1])+1+i
    print("val is ",val)
    Trans[content_updated[i]] = content_updated[i:val]
    print(Trans[content_updated[i]])
    i = i + val

Trans[content_updated[val]] = content_updated[val:i]
print(Trans[content_updated[val]])


# In[474]:


Trans_Name = {}

key = Trans.keys()
val = 0
for k in key:
    val = val + int(Trans[k][0].split(" ")[1])
    Trans[k] = Trans[k][::-1]
    p = Trans[k].pop()
    Trans_Name[k] = p.split(" ")[0]
    
    
    Trans[k].append("<START "+Trans_Name[k]+">")
    print("name ",Trans_Name[k])
    print(Trans[k])


# In[475]:


temp_var = {} # To store temp variables
HDD_temp_mapping = {} # HDD and memory Mapping

f= open(output,"w+")


def file_write(data):
    
    arrange = {} 
    new_list = []
    
    data1 = data.copy()
    
    for k in range(0, len(data1), 2):
        arrange[data1[k]] = data1[k+1]
        
    for k, v in sorted(arrange.items()):
        new_list.append(k)
        new_list.append(v)
        
    data2 = [str(i) for i in new_list]
    f.write(" ".join(data2))


def pre_access():
    if not Memory:
        pass
    else:
        file_write(Memory)
        print(Memory)
    
    f.write("\n")
    file_write(HDD)
    print("HDD",HDD)
    f.write("\n")
    

def process_Read(p, temp):
    
    for i in range(len(Memory)):
        if p == Memory[i]:
            val = int(Memory[i+1])
            temp_var[temp] = val
            return
            
    for i in range(len(HDD)):
        if p == HDD[i]:
            val = int(HDD[i+1])
            temp_var[temp] = val
            break
    
    if p not in Memory:
        Memory.append(p)
        Memory.append(val)
        

def memory_update(p, temp):
    for i in range(len(Memory)):
        if p == Memory[i]:
            Memory[i+1] = temp_var[temp]
            break
    
    
def process_Write(name, p, temp):
    
    for i in range(len(Memory)):
        if p == Memory[i]:
            val = int(Memory[i+1])
            break
    
    
    print("<",name,p,val,">")
    
    file_data = "<"+name+", "+str(p)+", "+str(val)+">"
    f.write(file_data)
    f.write("\n")
    
    HDD_temp_mapping[p] = temp
    memory_update(p, temp)
    pre_access()
    
def add_opn(temp, val):
    
    v = temp_var[temp]
    v = v + int(val)
    temp_var[temp] = v
    
def sub_opn(temp, val):
    
    v = temp_var[temp]
    v = v - int(val)
    temp_var[temp] = v
    
def mul_opn(temp, val):
    v = temp_var[temp]
    v = int(v) * int(val)
    temp_var[temp] = v
    
def HDD_update():
#     print("Ye HDD data ",HDD)
#     print("Ye Memory data ",Memory)
    
    for i in range(0,len(HDD),2):
        for j in range(len(Memory)):
        
            if HDD[i] == Memory[j]:
                HDD[i+1] = Memory[j+1]
            
    
def print_output(HDD_var):
    for i in range(len(HDD)):
        if HDD_var == HDD[i]:
            for j in range(len(Memory)):
                if HDD_var == Memory[j]:
                    HDD[i+1] = Memory[j+1]
                    return

    
    

COMMIT = []
count = 0
run = 0


while count < val:
    for k in key:
        
        run = 0
        while run < x or not Trans[k]:
            
            if not Trans[k]:
                print("<COMMIT ",Trans_Name[k],">")
                
                file_data = "<COMMIT "+Trans_Name[k]+">"
                f.write(file_data)
                f.write("\n")
                pre_access()
                break
            Inst = Trans[k].pop()
            
#             print("Inst is ",Inst)
            
            if 'START' in Inst:
                
                print(Inst)
                f.write(Inst)
                f.write("\n")
                pre_access()
                continue
            
            if 'READ' in Inst:
                var = Inst[Inst.find("(")+1:Inst.find(")")]
                process_Read(var.split(",")[0], var.split(",")[1])
                
            if 'WRITE' in Inst:
                var = Inst[Inst.find("(")+1:Inst.find(")")]
                process_Write(Trans_Name[k],var.split(",")[0], var.split(",")[1])
                
            if '+' in Inst:
                var = Inst.split("+")
        
                add_opn(var[0].split(" ")[0], var[1].split(" ")[0])
                
            if '-' in Inst:
                var = Inst.split("-")
                sub_opn(var[0].split(" ")[0], var[1].split(" ")[0])
                
            if '*' in Inst:
                var = Inst.split("*")
                mul_opn(var[0].split(" ")[0], var[1].split(" ")[0])
                
            if 'OUTPUT' in Inst:
                var = Inst.split("(")[1][0]
#                 HDD_update()
                print_output(var)
                
            run = run + 1
            
        count = count + run

print("Done")


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




